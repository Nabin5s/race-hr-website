<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('007_demands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_name');
            $table->string('company_name');
            $table->string('requirement');
            $table->date('start_date');
            $table->unsignedInteger('contract_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('007_demands');
    }
}
