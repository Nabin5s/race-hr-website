<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="hr in nepal,manpower in nepal,race international, top manpower of nepal, human resource jobs,hr consulting,staffing resource,workers,foreign employment" />
    <link rel="icon" href="{{URL::to('/')}}/fronts/images/title_logo.jpeg">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ config('app.name', 'Race International Human Resource Pvt. Ltd.') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/fronts/css/style-gallery.css">
    <link href="{{URL::to('/')}}/fronts/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('/')}}/fronts/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">

    <link href="{{URL::to('/')}}/fronts/css/font-awesome.css" rel="stylesheet">
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>
    <div id="app">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>

    <!-- Scripts -->
    <a href="{{URL::to('/')}}/fronts/#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <!-- js -->
    <script type="text/javascript" src="{{URL::to('/')}}/fronts/js/jquery-2.1.4.min.js"></script>

    <script type="text/javascript" src="{{URL::to('/')}}/fronts/js/bootstrap.js"></script>
    {{--<script src="{{ asset('js/app.js') }}"></script>--}}
</body>
</html>
