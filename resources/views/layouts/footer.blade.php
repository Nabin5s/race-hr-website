<!-- footer -->

<div>
    <div class="col-md-10"></div>


    <div class="social" id="social" style="display: inline-block">
        <span
            style="display: block;
            float: left;
            margin: 3px -4px 0 0;
            padding: 5px 10px;"><strong>Follow Us</strong>
        </span>&nbsp;&nbsp;

        <ul style="float: right">
            <li><a href="{{URL::to('/')}}/fronts/#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="{{URL::to('/')}}/fronts/#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="{{URL::to('/')}}/fronts/#"><i class="fa fa-rss"></i></a></li>
        </ul>
    </div>
</div>
<div class="footer_w3ls">
    <div class="container">
        @yield('footer-content')
        @include('layouts.footer_meta')

    </div>
</div>
<!-- //footer -->