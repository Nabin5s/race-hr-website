<div class="banner-bottom">
    <div class="container">
        <div class="tittle_head_w3ls">
            <h3 class="tittle">Introduction</h3>
        </div>
        <div class="inner_sec_grids_info_w3ls">
            <div class="col-md-1"></div>
            <div class="col-md-10 banner_bottom_left">

                <p><strong>Race International Human Resouce Pvt. Ltd.(RIHR)</strong> is one of the leading workforce recruitment agencies
                    in Kathmandu, Nepal which is legally registered under the Government of Nepal, Ministry of Labor
                    with License No: 1055/073/074 and managed by the team of highly competent and qualified professional.
                    The company competes in the employment services by offering and enhancing opportunities to all level
                    /categories of candidates from nepal.</p>

                <p><strong>RIHR</strong> itself is an <strong>EICC(Electronic Industry Citizenship Coalition)</strong> certified recruitment agency who has
                    ensured to provide appropriate service to both, Employer and Employee thus it has set the core value
                    with decorative mechanism, professionalism, integrity and quality as well as provides equal opportunity
                    at all levels with No Discrimination</p>

                <p>Since the establishment <strong>RIHR</strong> succeeded to satisfy and getting positive feedback from abroad based clients
                    throughout its' service by understanding and fulfilling the requirements of Skilled, Semi-Skilled,
                    Unskilled and Professional personal as per criteria set by Clients in Malaysia, Qatar, U.A.E, Oman,
                    Kuwait, Bahrain and many more.</p>

                <p>Referring to our corporate slogan <span style="color: #43B852">"Enhancing Opportunity"</span> <strong>RIHR</strong> has successfully identifying the ability
                    of reverent candidates and arranging trade test(s) / training(s) and interview(s) as well as other
                    assistance if needed which prepare our candidates to cope up with more and more opportunities in the
                    field of froreign employment..</p>
                <div class="clearfix"> </div>
            </div>
        <!-- <div class="col-md-6 banner_bottom_right">
					<div class="agileits_w3layouts_banner_bottom_grid">
						<img src="{{URL::to('/')}}/fronts/images/ab.png" alt=" " class="img-responsive" />
					</div>
				</div> -->
            <div class="clearfix"> </div>
            <div class="col-md-1"></div>
        </div>

    </div>
</div>
<!-- //banner-bottom -->