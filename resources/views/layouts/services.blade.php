<!-- /mid-services -->
<div class="mid_services" xmlns:display="http://www.w3.org/1999/xhtml">
    <div class="col-md-8 according_inner_grids" style=" margin: auto;width: 60%;padding: 10px;
    box-shadow: 0px 0px 0px 16px rgba(61, 149, 60, 0.3);">
        <h3 class="agile_heading two">Contact Us</h3>
        <form action="{{URL::to('/contact')}}" method="post" id="fronPageForm">
            <input name="_token" type="hidden" value="{{csrf_token()}}">
            <div class="form-group col-md-6">
                <input type="name" name="name" class="form-control" id="name" placeholder="Full Name" required>
            </div>
            <div class="form-group col-md-6">
                <input type="email" name="email" class="form-control" id="email" placeholder="info@race-hr.com" required>
            </div>
            <div class="form-group col-md-6">
                <input type="subject" name="subject" class="form-control" id="subject" placeholder="Subject" required>
            </div>
            <div class="form-group col-md-6">
                <input type="phone" name="phone" class="form-control" id="phone" placeholder="Telephone Number" required>
            </div>
            <div class="form-group col-md-12">
                <textarea class="form-control" name="fullMessage" id="message" rows="3"></textarea>
            </div>
            <div class="form-group col-md-12" style="margin:0px auto;text-align: center">
                <input type="submit" name="submit" class="btn btn-success btn-lg active" value="Send">
            </div>
        </form>
    </div>
</div>
<div class="clearfix"> </div>
</div><br><br>
<!-- //mid-services -->