<!-- header -->
<div class="header" id="home">
    <div class="content white agile-info">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <!-- <a class="navbar-brand" href="{{URL::to('/')}}/fronts/index.html">
							<h1><span class="fa fa-signal" aria-hidden="true"></span> RIHR <label>Hr Agency</label></h1>
						</a> -->
                    <a href="{{URL::to('/')}}">
                        <img src="{{URL::to('/')}}/fronts/images/logo.png" alt=" " class="rounded mx-auto d-block" height="150" width="150" />
                    </a>
                </div>

                <div>
                    <p style="text-align: center; font-family: 'Tangerine', serif; font-size: 2.9em;color: #43B852"><strong>"Enhancing Opportunity"</strong></p>
                </div>
                <br>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="link-effect-2" id="link-effect-2">
                        <ul class="nav navbar-nav">
                            <li><a href="{{URL::to('/')}}" class="effect-3">Home</a></li>
                            <li><a href="{{URL::to('/')}}/demand" class="effect-3">services</a></li>

                            <li class="dropdown">
                                <a href="{{URL::to('/')}}/fronts/services.html" class="dropdown-toggle effect-3" data-toggle="dropdown">Profile <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                <!-- <li><a href="{{URL::to('/')}}/services">About Us</a></li> -->
                                    <li class="divider"></li>
                                    <li><a href="{{URL::to('/')}}/message_from_md">Message Form MD</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{URL::to('/')}}/mission_vision_value">Mission / Vision / Value</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">What We Do</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{URL::to('/')}}/what_we_serve">What We Serve</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Requirement Procedure</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{URL::to('/')}}/required_document">Required Documentation</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Organization chart</a></li>
                                </ul>
                            </li>
                            <li><a href="{{URL::to('/')}}/legal_status" class="effect-3">Legal Status</a></li>
                            <li><a href="#" class="effect-3">gallery</a></li>
                            <li><a href="{{URL::to('/')}}/contact" class="effect-3">find us</a></li>
                        </ul>
                    </nav>
                </div>
                <!--/.navbar-collapse-->
                <!--/.navbar-->
            </div>
        </nav>
    </div>
</div>
