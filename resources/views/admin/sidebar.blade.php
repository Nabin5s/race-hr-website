<div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="{{URL::to('/')}}/admin" class="simple-text">
            ADMIN HOME
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class={{ request()->is('admin/demands') ? 'active' : '' }}>
                <a href="{{ route('demands') }}">
                    <i class="material-icons">content_paste</i>
                    <p>Insert Demand</p>
                </a>
            </li>
            <li class={{ request()->is('admin/demands/list') ? 'active' : '' }}>
                <a href="{{ route('showDemands') }}">
                    <i class="material-icons">content_paste</i>
                    <p>Demand List</p>
                </a>
            </li>
            <li class={{ request()->is('admin/clients') ? 'active' : '' }}>
                <a href="{{ route('clients') }}">
                    <i class="material-icons">library_books</i>
                    <p>Insert Client</p>
                </a>
            </li>
            <li class={{ request()->is('admin/clients/list') ? 'active' : '' }}>
                <a href="{{ route('showClients') }}">
                    <i class="material-icons">bubble_chart</i>
                    <p>Client List</p>
                </a>
            </li>
        </ul>
    </div>
</div>
