@extends('admin.layout')

@section('adminContent')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Demand List</h4>

                    </div>
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                            <th>SNo.</th>
                            <th>Client Name</th>
                            <th>Client Image</th>
                            <th>Action</th>



                            </thead>
                            <tbody>
                            <?php
                            $sn=1;
                            ?>
                            @foreach($client_list as $client)
                                <tr>
                                    <td>{{$sn++}}</td>
                                    <td>{{$client->client_name}}</td>
                                    <td><img  src="{{URL::to('/')}}/uploads/{{$client->image}}" height="75px" width="75px" ></td>
                                    <form class="" method="get" action="{{URL::to('delete_client/'.$client->id)}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <td>
                                            <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure you want to delete it ?');">Delete</button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection