@extends('admin.layout')

@section('adminContent')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Demand List</h4>

                    </div>
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                            <th>SNo.</th>
                            <th>Country Name</th>
                            <th>Company Name</th>
                            <th>Requirements</th>
                            <th>Start Date</th>
                            <th>Contract Year</th>
                            <th>Action</th>


                            </thead>
                            <tbody>
                            <?php
                            $sn=1;
                            ?>
                            @foreach($demand_list as $demand)
                                <tr>
                                    <td>{{$sn++}}</td>
                                    <td>{{$demand->country_name}}</td>
                                    <td>{{$demand->company_name}}</td>
                                    <td>{{$demand->requirement}}</td>
                                    <td>{{$demand->start_date}}</td>
                                    <td>{{$demand->contract_year}}</td>
                                    <form class="" method="get" action="{{URL::to('delete/'.$demand->id)}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <td>
                                            <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure you want to delete it ?');">Delete</button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection