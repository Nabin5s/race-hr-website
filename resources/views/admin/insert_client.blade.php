@extends('admin.layout')

@section('adminContent')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Insert Client</h4>

                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <p class="error_item">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form action="{{ route('storeClients') }}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" value="{{csrf_token()}}" name="_token">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Client Name</label>
                                        <input type="text" class="form-control" name="client_name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Insert Image</label>
                                        <input type="file" class="form-control" name="client_image">
                                    </div>
                                </div>
                            </div>

                            <input type="submit" name="submit" class="btn btn-primary pull-right">
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection