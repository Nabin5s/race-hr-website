@extends('admin.layout')

@section('adminContent')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Enter Demand</h4>

                    </div>
                    <div class="card-content">
                        <form action="{{route('storeDemands')}}" method="POST">
                            <input type="hidden" value="{{csrf_token()}}" name="_token">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Country Name</label>
                                        <input type="text" class="form-control" name="country_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Company Name</label>
                                        <input type="text" class="form-control" name="company_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Number Of Requirement</label>
                                        <input type="text" class="form-control" name="requirement">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Start Date (yyyy/mm/dd)</label>
                                        <input type="text" class="form-control" name="start_date">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Contract Year</label>
                                        <input type="text" class="form-control" name="contract_year">
                                    </div>
                                </div>
                            </div>
                            <input type="submit" name="submit" class="btn btn-primary pull-right">
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection