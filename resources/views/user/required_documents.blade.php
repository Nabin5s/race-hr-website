@extends('layouts.app')

@section('content')
    <!-- /inner_content -->
    <div class="inner_content_info_agileits">
        <div class="container">
            <div class="tittle_head_w3ls">
                <h3 class="tittle">Required Documentation </h3>
            </div>
            <div class="inner_sec_grids_info_w3ls">
                <div class="col-md-12 job_info_left">
                    <div class="tab_grid_prof">
                        <div>
                            <br>
                            <p><strong>In order to recruit Nepalese workers, Department of Foreign employment (DOFE) in
                                    Nepal and Embassies / Consulates of the country concerned have implemented certain
                                    regulation and documentation procedure. Thus, the employers are required to provide
                                    us with following documents :-</strong></p>
                        </div>


                        <div class="col-sm-9">

                            <ol>
                                <br>
                                <li><h4>Demand Letter</h4></li>
                                <p>This principle document should clearly state all the requirement details and
                                    recruitment terms and conditions such as required number of workes, their
                                    qualification / criteria, salary, accommodation, transportation, contract period,
                                    working hours, food and other provisions.</p>
                                <br>
                                <li><h4>Power of Attorney</h4></li>
                                <p>This principle document gives us authority to complete all the recruitment formalities
                                    at the embassies airports, labour office or wherever seems necessary. Followings are
                                    some samples of "Power of Attorney".</p>
                                <br>
                                <li><h4>Employment Agreement Paper</h4></li>
                                <p>This is the legal contract between the employer and the employee. The agreement paper
                                    should describe all the major terms and conditions of the employment such position,
                                    salary. accommodation, transportation, contract period, working hours, food and
                                    other provisions as per the requirement of DOFE in Nepal and as per the labour of
                                    the country of employment.</p>
                                <br>
                                <li><h4>Recruitment Agreement</h4></li>
                                <p>This is the formal agreement signed by both the employer and recruiter. It should be
                                    prepared in the letter head of the employer and should be signed by the authorized
                                    persons of both the parties (employer and HJE Nepal)</p>
                                <br>
                                <li><h4>Affidavit/Sworn in statement</h4></li>
                                <p>This id the commitment letter from Employer accepting all the terms and condition
                                    stated in the demand letter and in agreement paper. Breach of the terms ans
                                    condition of the letter will black list the employer for the future recruitment from
                                    Nepal as well as legal action against the employer in the country of employment.</p>
                                <br>
                                <li><h4>Employment Agreement Paper</h4></li>
                                <p>This is the commitment letter provided to the DOFE in Nepal with regard to the
                                    deployment if Neplease workers in the said countries and companies only.</p>

                            </ol>
                        </div>
                        <div class="col-sm-3 loc_1">
                            <img src="{{URL::to('/')}}/fronts/images/demand.png" alt="high skilled">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- //inner_content -->
@endsection
@section('footer-content')
    @include('layouts.footer_slider')
@endsection