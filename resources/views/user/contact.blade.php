@extends('layouts.app')

@section('content')
    <!-- /inner_content -->
    <div class="inner_content_info_agileits">
        <div class="container">
            <div class="tittle_head_w3ls">
                <h3 class="tittle">Contact Us</h3>
            </div>
            <br>
            <div>
                <p style="text-align: center;">kindly drop your email or directly contact us at following numbers for additional information.</p>
            </div>
            <br>
            <br>
                @include('layouts.services')
            <div class="inner_sec_grids_info_w3ls>
                <div class="col-md">
                    <div class="clearfix"> </div>
                    </div>
                <br>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14129.898435490135!2d85.3466715!3d27.7026289!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4c4f2a7734037dca!2sRACE+International+HR!5e0!3m2!1sen!2snp!4v1525154067873" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
                <div class="container" style="color: #fff;background-color: #43b852">
                    <div class="col-md-4 agile_info_mail_img_info">
                        <div class="">
                            <label>OFFICE ADDRESS</label>
                        </div>
                        <div>
                            <img src="{{URL::to('/')}}/fronts/images/contact.png" class="image-responsive">
                        </div>
                    </div>
                    <div class="col-md-4 agile_info_mail_img_info">
                        <div class="">
                            <label>KEY PERSON</label>
                            <br>
                            <label>INTERNATIONAL SECTOR</label>
                            <div>
                                <p>MR. JAY SHAH</p>
                                <P>(Managing Director)</P>
                            </div>
                            <div>
                                <p>Cell No.:  +977-9851170700 </p>
                            </div><br>
                        </div>
                        <br>
                        <div>
                            <div>
                                <p>MS. MANJU BASNET</p>
                                <P>(Secretary Cum Admin Manager)</P>
                            </div>
                            <div>
                                <p>Cell No.: +977-9801101805</p>
                            </div><br>
                        </div>

                    </div>
                    <div class="col-md-4 agile_info_mail_img_info">
                        <br><br>
                        <div class="">
                            <label>DOMESTIC SECTOR</label>
                            <br>
                            <div>
                                <p>MR. RAMDHYAN SAH</p>
                                <p>(DIRECTOR)</p>
                            </div>
                            <div>
                                <p>Cell No.: +977-9801101803 </p>
                            </div><br>

                        </div>
                        <br>
                        <div>
                            <div>
                                <p>MR. DIPAK BASNET</p>
                                <P>(PUBLIC RELATION OFFICER) </P>
                            </div>
                            <div>
                                <p>Cell No.: +977-9801101806 </p>
                            </div><br>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    {{--google map--}}

@endsection

@section('footer-content')
    @include('layouts.footer_slider')
@endsection
