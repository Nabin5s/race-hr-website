@extends('layouts.app')

@section('content')
    <!-- /inner_content -->
    <div class="inner_content_info_agileits">
        <div class="container">
            <div class="tittle_head_w3ls">
                <h3 class="tittle">What we serve </h3>
            </div>
            <div class="inner_sec_grids_info_w3ls">
                <div class="col-md-12 job_info_left">
                    <div class="tab_grid_prof">
                        <div>
                            <p><strong>We serve in providing appropriate candidates for employment to the destination countries/clients available in Nepal which are categories into three groups such as Highly Professional, Skilled Technician, Semi-Skilled/Unskilled.</strong></p><br><br>
                        </div>
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <br>
                                    <h3>Highly Professional</h3>
                                    <ul class='fa-ul'>
                                        <li><i class="fa-li fa fa-plus"></i>Engineering (Civil, Electrical, Mechanical, Chemical)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Architects / Planners</li>
                                        <li><i class="fa-li fa fa-plus"></i>Doctors (Specialists, General Physicians, Dentists, Surgeon)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Professor (College, School Teacher)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Banker and Financial Service staffs</li>
                                        <li><i class="fa-li fa fa-plus"></i>IT Personnel</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{URL::to('/')}}/fronts/images/hp.png" alt="high skilled" class="img-responsive">
                                </div>
                            </div>
                            <div>
                                <div class="col-md-4">
                                    <img src="{{URL::to('/')}}/fronts/images/st.png" alt="skilled technician" class="img-responsive">
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <h3>Skilled Technician</h3>
                                    <ul class='fa-ul'>
                                        <li><i class="fa-li fa fa-plus"></i>Junior Engineer / Technician</li>
                                        <li><i class="fa-li fa fa-plus"></i>Supervisor, Overseer, Estimators</li>
                                        <li><i class="fa-li fa fa-plus"></i>Nurses (Male, Female)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Pharmacists, Laboratory Technicians, Medical Assistants</li>
                                        <li><i class="fa-li fa fa-plus"></i>Sales and Marketing Personnel</li>
                                        <li><i class="fa-li fa fa-plus"></i>Plant Operator (Electric Mechanical)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Foreman (Electrical / Mechanical)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Trade Workers (Carpenter / Steel fixer / Rigger / Mason / Welder / Plumber)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Drivers (Heavy / Light / trailer Operator)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Hospitality and service sector (Waiter / Waitress / Bakers / Bar Attendant etc.)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Construction Equipment Operators</li>
                                        <li><i class="fa-li fa fa-plus"></i>Industrial Sectors (Machine Operators)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Sales and Marketing</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <h3>Semi Skilled / Unskilled</h3>
                                    <ul class='fa-ul'>
                                        <li><i class="fa-li fa fa-plus"></i>Factory (General Factory Workers)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Cleaning and Hygiene (Cleaners / Sweepers)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Security Sectors (Guards / Watchman)</li>
                                        <li><i class="fa-li fa fa-plus"></i>Factory, Industrial workers</li>
                                        <li><i class="fa-li fa fa-plus"></i>Farm and Agriculture workers</li>
                                        <li><i class="fa-li fa fa-plus"></i>Peons / Office Boy / Store Keeper / Office Clerk</li>
                                        <li><i class="fa-li fa fa-plus"></i>Hospital, Nursing, Laboratory, Pharmacists and Medical Assicatants</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{URL::to('/')}}/fronts/images/ss.png" alt="high skilled" class="img-responsive">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //inner_content -->
@endsection
@section('footer-content')
    @include('layouts.footer_slider')
@endsection