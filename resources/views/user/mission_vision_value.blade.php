@extends('layouts.app')

@section('content')
    <!-- /inner_content -->
    <div class="inner_content_info_agileits">
        <div class="container">
            <div class="tittle_head_w3ls">
                <h3 class="tittle">Mission, Vision and Value</h3>
            </div>
            <div class="inner_sec_grids_info_w3ls">
                <div class="col-md-12 job_info_left">
                    <div class="tab_grid_prof">

                        <div class="col-sm-9">
                            <h3>MISSION</h3>
                            <p>Our mission is to uphold the standard of personal and professional ethics, honesty and trust</p><br>
                            <h3>VISION</h3>
                            <p>Our vision is to be the most reliable and trusted Manpower Recruitment Agency in Nepal</p><br>
                            <h3>VALUE</h3>
                            <p>Professionalism, teamwork, integrity, quality and commitment is our value</p>
                        </div>
                        <div class="col-sm-3 loc_1">
                            <img src="{{URL::to('/')}}/fronts/images/mission.png" alt=""></a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- //inner_content -->
@endsection
@section('footer-content')
    @include('layouts.footer_slider')
@endsection