@extends('layouts.app')

@section('content')
    @include('layouts.carousel')
    @include('layouts.banner')
    @include('layouts.services')
@endsection
@section('footer-content')
@include('layouts.footer_slider')
@endsection