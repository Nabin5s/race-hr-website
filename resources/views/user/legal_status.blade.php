@extends('layouts.app')

@section('content')
    <!-- /inner_content -->
    <div class="inner_content_info_agileits">
        <div class="container">
            <div class="tittle_head_w3ls">
                <h3 class="tittle">Legal Status</h3>
            </div>
            <div class="inner_sec_grids_info_w3ls">
                <div class="col-md-12">
                    <img src="{{URL::to('/')}}/fronts/images/certificate.jpg" class="img-thumbnail" alt="certificate" >
                </div>
                <div class="col-md-6">
                    <div>
                        <img src="{{URL::to('/')}}/fronts/images/c1.jpg" class="img-thumbnail" alt="certificate">
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <img src="{{URL::to('/')}}/fronts/images/c2.jpg" class="img-thumbnail" alt="certificate">
                    </div>
                </div>
                {{--second line image--}}
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div>
                            <img src="{{URL::to('/')}}/fronts/images/c3.jpg" class="img-thumbnail" alt="certificate">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            <img src="{{URL::to('/')}}/fronts/images/c4.jpg" class="img-thumbnail" alt="certificate">
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //inner_content -->
@endsection
@section('footer-content')
    @include('layouts.footer_slider')
@endsection