@extends('layouts.app')

@section('content')
    <br><br><br>
    <div class="container">
        <div class="tittle_head_w3ls">
            <h3 class="tittle">Recent Jobs</h3>
        </div>
        <div class="inner_sec_grids_info_w3ls">
            <br>
            @if(count($demand_list)>0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">S.N.</th>
                    <th scope="col">Country Name</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">No. of requirements</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Contract Year</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $sn=1;
                ?>
                    @foreach($demand_list as $demand)
                        <tr>
                            <td>{{$sn++}}</td>
                            <td>{{$demand->country_name}}</td>
                            <td>{{$demand->company_name}}</td>
                            <td>{{$demand->requirement}}</td>
                            <td>{{$demand->start_date}}</td>
                            <td>{{$demand->contract_year}}</td>
                        </tr>
                    @endforeach
                @else
                    <p class="alert alert-danger">No job demands, please visit us later.</p>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer-content')
    @include('layouts.footer_slider')
@endsection
