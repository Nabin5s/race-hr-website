
<!doctype html>
<html>
<head>
    <title>Race International HR</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body>
<div>
    <h1>You have received an email message through website!</h1>
    <p>Below is the information we received from website visitor:</p>
    <ul>
        <li>Full Name: <span>{{ $name }}</span></li>
        <li>Subject: <span>{{ $subject }}</span></li>
        <li>Email: <span>{{ $email }}</span></li>
        <li>Telephone: <span>{{ $phone }}</span></li>
        <li>Message: <span>{{ $fullMessage }}</span></li>
    </ul>
    <p>We have received visitor's message and please respond to this message.</p>
    <p><a href="http://www.race-hr.com">Click Here for more information about Race International Human Resource Pvt. Ltd.</a></p>
</div>
</body>
</html>
