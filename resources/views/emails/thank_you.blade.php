
<!doctype html>
<html>
<head>
    <title>Race International HR</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body>
<div>
    <h1>Thank You {{ $name }}!</h1>
    <p>Below is the information we received from you:</p>
    <ul>
        <li>Subject: <span>{{ $subject }}</span></li>
        <li>Telephone: <span>{{ $phone }}</span></li>
        <li>Message: <span>{{ $fullMessage }}</span></li>
    </ul>
    <p>We have received your message and someone from our company will contact you soon.</p>
    <p><a href="http://www.race-hr.com">Click Here for more information about Race International Human Resource Pvt. Ltd.</a></p>
</div>
</body>
</html>
