<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '007_demands';
}
