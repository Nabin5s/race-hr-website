<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Validator;
use App\Client;
use App\Demand;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    public function demands()
    {
        $demand_list = Demand::all();
        return view('admin.insert_demand',compact('demand_list'));
    }

    public function showDemands()
    {
        $demand_list = Demand::all();
        return view('admin.demand_list',compact('demand_list'));
    }

    public function storeDemands(Request $request)
    {
        $demand = new Demand();
        $demand->country_name = $request->country_name;
        $demand->company_name = $request->company_name;
        $demand->requirement = $request->requirement;
        $demand->start_date = $request->start_date;
        $demand->contract_year = $request->contract_year;
        $result = $demand->save();

        if ($result) {
            return redirect('admin/demands')->with('status', 'Demand saved!');
        } else {
            return redirect('admin/demands')->with('status', 'Demand save failed!');
        }
    }

    public function clients()
    {
        return view('admin.insert_client');
    }

    public function showClients()
    {
        $client_list = Client::all();
        return view('admin.client_list',compact('client_list'));
    }

    public function storeClients(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect('admin/clients')
                ->withErrors($validator)
                ->withInput();
        }

        $client = new Client();
        $client->client_name = $request->client_name;
        $client->image = $this->upload_image($request);
        $result = $client->save();

        if ($result) {
            return redirect('admin/clients')->with('status', 'Client saved!');
        } else {
            return redirect('admin/demands')->with('status', 'Client save failed!');
        }
    }

    public function upload_image(Request $request)
    {
        $file = $request->file('client_image');
        $name = $file->getClientOriginalName();

        //create thumbs
        $thumbImage = Image::make($file->getRealPath())->fit(400, 300, function ($constraint) {
            $constraint->upsize();
        });
        $thumbImage->save(public_path('/uploads/thumbs').'/'.$name, 80);

        $file->move(public_path('/uploads'), $name);

        return $name;
    }
    public function destroy($id)
    {
        $demand=Demand::findorfail($id);
        $demand->delete();
        return redirect('admin/demands/list');
    }
    public function destroy_client($id)
    {
        $demand=Client::findorfail($id);
        $demand->delete();
        return redirect('admin/clients/list');
    }

}
