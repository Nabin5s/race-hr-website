<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\ThankYouForContactingUs;
use App\Mail\MessageFromWebsite;
use Illuminate\Http\Request;
use App\Client;
use App\Demand;
use App\MailStorage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_list = Client::all();
        return view('user.home', compact('client_list'));
    }

    public function message()
    {
        $client_list = Client::all();
        return view('user.message', compact('client_list'));
    }

    public function missionVisionValue()
    {
        $client_list = Client::all();
        return view('user.mission_vision_value', compact('client_list'));
    }

    public function whatWeServe()
    {
        $client_list = Client::all();
        return view('user.what_we_serve', compact('client_list'));
    }

    public function requiredDocuments()
    {
        $client_list = Client::all();
        return view('user.required_documents', compact('client_list'));
    }

    public function legalStatus()
    {
        $client_list = Client::all();
        return view('user.legal_status', compact('client_list'));
    }

    public function demand()
    {
        $client_list = Client::all();
        $demand_list = Demand::all();
        return view('user.demand',compact(['demand_list', 'client_list']));
    }

    public function contact()
    {
        $client_list = Client::all();
        return view('user.contact', compact('client_list'));
    }

    public function sendMail(Request $request)
    {
        // Store in DB
        $mail = new MailStorage();
        $mail->name = $request->name;
        $mail->email = $request->email;
        $mail->phone = $request->phone;
        $mail->subject = $request->subject;
        $mail->message = $request->fullMessage;
        $mail->ip_address = $request->ip();
        $mail->save();
        // Send to Race HR
        $feedback = new MessageFromWebsite($request);
        Mail::to('nabin5s@hotmail.com')->send($feedback);
        // Thank the submitter
        $thankYou = new ThankYouForContactingUs($request);
        Mail::to($request->email)->send($thankYou);
        return redirect('contact')->with('status', 'Message sent!');
    }
}
