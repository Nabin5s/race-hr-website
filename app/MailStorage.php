<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailStorage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '007_mails';
}
