<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\ThankYouForContactingUs;

Auth::routes();
Route::get('logout','Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeController@index')->name('home');

Route::get('message_from_md', 'HomeController@message');
Route::get('mission_vision_value', 'HomeController@missionVisionValue');
//TODO What we do route
Route::get('what_we_serve', 'HomeController@whatWeServe');
//TODO Req procedute route
Route::get('required_document', 'HomeController@requiredDocuments');
//TODO organization charset route
Route::get('legal_status', 'HomeController@legalStatus');
Route::get('demand', 'HomeController@demand');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::post('contact', 'HomeController@sendMail');

Route::get('delete/{id}','AdminController@destroy');
Route::get('delete_client/{id}','AdminController@destroy_client');




Route::middleware(['auth'])->prefix('admin')->group(function () {

        Route::get('/','AdminController@index'); //admin menu

        Route::get('/demands','AdminController@demands')->name('demands'); //load demands form
        Route::get('/demands/list','AdminController@showDemands')->name('showDemands'); //show list of demands
        Route::post('/demands/store','AdminController@storeDemands')->name('storeDemands'); //post to store demands

        Route::get('/clients','AdminController@clients')->name('clients');
        Route::get('/clients/list','AdminController@showClients')->name('showClients');
        Route::post('/clients/store','AdminController@storeClients')->name('storeClients');



});

//Route::resource('admin', 'AdminController');

Route::get('mail', function () {
   Mail::to('nabin5s@hotmail.com')->send(new ThankYouForContactingUs);
});