Laravel based CMS website with admin login for CRUD operations and frontend to display changes.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
